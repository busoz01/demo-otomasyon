﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UnitTesting.Logging;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using TechTalk.SpecFlow;

namespace Deneme.StepBinding
{
    [Binding]
    public class TestlerSteps
    {
        private static IWebDriver driver = Conf.GetDriver();

        [When(@"I LoginPage")]
        public void WhenILoginPage()
        {
            LoginPage login = new LoginPage(driver);
            login.TypeFirstName();
            login.TypeLastName();
            login.TypeEmail();
            login.TypePassword();
            login.Typecheckbox();
            login.TypeLLoginButton();
        }

        [Then(@"I go to url contains ""(.*)""")]
        public void WhenIGoToContains(string url)
        {
            string URL = (ConfigurationManager.AppSettings["url"] + url);
            Assert.IsTrue(URL.Contains(url));

        }
        [Then(@"I go to url  ""(.*)""")]
        public void WhenIGoTo(string url)
        {
            string URL = (ConfigurationManager.AppSettings["url"] + url);
        }
        [Given(@"I go to Url")]
        public void NavigateToTodoList()
        {
            var environment = ConfigurationManager.AppSettings["url"];
            driver.Navigate().GoToUrl(environment);
        }
        [When(@"I entered ""(.*)""")]
        public void WhenIEntered(string button)
        {
            IWebElement buton = driver.FindElement(By.CssSelector(button));
            buton.Click();
        }
        [When(@"I change ""(.*)"" selected element ""(.*)""")]
        public void WhenIChange(string htmlText, string value)
        {
            var select = driver.FindElement(By.CssSelector(htmlText));
            var selectElement = new SelectElement(select);
            selectElement.SelectByText(value);
            return;
        }

        [When(@"I Fill ""(.*)"" text ""(.*)""")]
        public void WhenIFill(string htmlText, string text)
        {
            WaitUntilElementVisible(By.CssSelector(htmlText));
            driver.FindElement(By.CssSelector(htmlText)).Clear();
            IWebElement text1 = driver.FindElement(By.CssSelector(htmlText));
            text1.SendKeys(text);

        }
        public static IWebElement WaitUntilElementVisible(By elementLocator, int timeout = 10)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                return wait.Until(ExpectedConditions.ElementIsVisible(elementLocator));
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Element with locator: '" + elementLocator + "' was not found.");
                throw;
            }
        }
        [Then(@"I should see ""(.*)"" text is ""(.*)""")]
        public void ThenIShouldSeeTextIs(string htmlText, string text)
        {
            var a = driver.FindElement(By.CssSelector(htmlText)).Text;
            Assert.IsTrue(a.Equals(text));
        }
        public string a = "";
        [When(@"I get text ""(.*)""")]
        public void Gettext(string element)
        {
            a = driver.FindElement(By.CssSelector(element)).Text;
            if (string.IsNullOrEmpty(a))
            {
                Assert.IsFalse(false);
            }
        }
        [When(@"get text equals element ""(.*)""")]
        public void WhenGetText(string element)
        {
            var findelement = driver.FindElement(By.CssSelector(element)).Text;
            Assert.IsTrue(findelement.Equals(a));
        }

        [Then(@"I should see element ""(.*)""")]
        public void IShouldSeeElement(string findElement)
        {
            WaitUntilElementExists(By.CssSelector(findElement));
        }
        public static IWebElement WaitUntilElementExists(By elementLocator, int timeout = 10)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeout));
                return wait.Until(ExpectedConditions.ElementExists(elementLocator));
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Element with locator: '" + elementLocator + "' was not found in current context page.");
                throw;
            }
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }

}
