﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deneme.StepBinding
{
    public class LoginPage
    {
        IWebDriver driver;
        By FirstName = By.CssSelector("div:nth-child(3) > input");
        By LastName = By.CssSelector("div.register > div:nth-child(4) > input");
        By Email = By.CssSelector("div:nth-child(5) > input");
        By Password = By.CssSelector("#password:nth-child(1)");
        By checkbox = By.CssSelector("div.custom-checkbox");
        By LoginButton = By.CssSelector("div:nth-child(12) > input");
        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
        }
        public void TypeFirstName()
        {
            driver.FindElement(FirstName).SendKeys("test deneme");
        }
        public void TypeLastName()
        {
            driver.FindElement(LastName).SendKeys("test deneme1");
        }
        public void TypeEmail()
        {
            driver.FindElement(Email).SendKeys("testdeneme1@gmail.com");
        }
        public void TypePassword()
        {
            driver.FindElement(Password).SendKeys("testdeneme1");
        }
        public void Typecheckbox()
        {
            driver.FindElement(checkbox).Click();
        }
        public void TypeLLoginButton()
        {
            driver.FindElement(LoginButton).Click();
        }
    }
}
